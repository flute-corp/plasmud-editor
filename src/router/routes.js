
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'index', component: () => import('pages/Index.vue') },
      { path: 'create/blueprints', name: 'blueprints', component: () => import('pages/Blueprints.vue') },
      { path: 'editor/map', name: 'map', component: () => import('pages/Map.vue') },
      { path: 'editor/script', name: 'script', component: () => import('pages/Editor.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
