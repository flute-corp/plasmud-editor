/**
 * Capitalize le début d'une chaine
 * @param s
 * @returns {string}
 */
export function capitalize (s) {
  return s[0].toUpperCase() + s.slice(1).toLowerCase()
}

/**
 * Capitalize chaque mot séparé par un caractère défini
 * @param s {string}
 * @param separator {string}
 * @returns {string}
 */
export function capitalizeAll (s, separator = ' ') {
  const aListString = s.split(separator)
  const aReturnString = []
  for (const string of aListString) {
    aReturnString.push(capitalize(string))
  }
  return aReturnString.join(' ')
}
