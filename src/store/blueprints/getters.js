export const getSelectBlueprint = state => state.select.blueprint

export const getRefTypes = state => state.refs.types
export const getRefSubtypes = state => state.refs.subtypes
export const getRefSubtypesRequest = state => state.refs.subtypes.map(s => Object.keys(s)[0])
export const getRefRaces = state => state.refs.races
export const getRefGenders = state => state.refs.genders
export const getRefRanks = state => state.refs.ranks

export const getBlueprints = state => state.blueprints

export const getTreeBlueprints = state => {
  return [
    {
      name: 'Objets',
      icon: 'fas fa-hammer',
      children: state.blueprints.filter(b => b.type === 'item'),
      disable: true
    },
    {
      name: 'Objets placeables',
      icon: 'fas fa-hand-paper',
      children: state.blueprints.filter(b => b.type === 'placeable'),
      disable: true
    },
    {
      name: 'Créatures',
      icon: 'fab fa-qq',
      children: state.blueprints.filter(b => b.type === 'actor'),
      disable: true
    }
  ]
}
