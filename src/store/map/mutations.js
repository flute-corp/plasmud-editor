import ArrayHelper from 'src/libs/array-helper'

/**
 * @param state
 * @param value {number}
 */
export function setMouseX (state, value) {
  state.mouse.x = value
}

/**
 * @param state
 * @param value {number}
 */
export function setMouseY (state, value) {
  state.mouse.y = value
}

/**
 * @param state
 * @param value {number}
 */
export function setCoordX (state, value) {
  state.mouse.cx = value
}

/**
 * @param state
 * @param value {number}
 */
export function setCoordY (state, value) {
  state.mouse.cy = value
}

/**
 * @param state
 * @param value {number}
 */
export function setMapWidth (state, value) {
  state.map.w = value
}

/**
 * @param state
 * @param value {number}
 */
export function setMapHeight (state, value) {
  state.map.h = value
}

/**
 * @param state
 * @param door {object}
 */
export function addDoor (state, door) {
  state.doors.push(door)
}

/**
 * @param state
 * @param door {object}
 */
export function deleteDoor (state, door) {
  ArrayHelper.remove(state.doors, door)
}

/**
 * @param state
 * @param door {object}
 */
export function updateDoor (state, door) {
  ArrayHelper.updateElement(state.doors, door)
}

/**
 * @param state
 * @param doors {[]}
 */
export function setDoors (state, doors) {
  ArrayHelper.update(state.doors, doors)
}
