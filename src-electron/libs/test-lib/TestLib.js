class TestLib {
  constructor () {
    this._message = ''
  }

  get message () {
    return this._message
  }

  set message (sMessage) {
    this._message = sMessage
  }
}

module.exports = TestLib
