const fs = require('fs/promises')

class FsManager {
  /**
   * Liste un répertoire
   * @param path {string} chemin du répertoire
   * @return {Promise<string[]>}
   */
  ls (path) {
    return fs.readdir(path)
  }

  /**
   * @param path {string} chemin du répertoire
   * @param ext {string} extension sans le point
   * @return {Promise<string[]>}
   */
  async lsExt (path, ext) {
    const oDir = await this.ls(path)
    return oDir.filter(file => file.substr(-3) === '.' + ext)
  }
}

module.exports = FsManager
