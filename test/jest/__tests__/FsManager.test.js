const FsManager = require('../../../src-electron/libs/fs-manager/FsManager.js')

describe('Je veux tester la class permettant la gestion de repertoire', function () {
  it('je veux tester un ls dans un répertoire', async function () {
    const fsm = new FsManager()
    const oDir = await fsm.ls('/')
    expect(oDir.includes('usr')).toBeTruthy()
  })
  it('je veux afficher les fichiers avec une extension particilère', async function () {
    const fsm = new FsManager()
    const aListFile = await fsm.lsExt('./', 'js')
    expect(aListFile.includes('jest.config.js')).toBeTruthy()
  })
})
