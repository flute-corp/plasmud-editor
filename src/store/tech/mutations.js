/**
 * @param state
 * @param rightPanel {boolean}
 */
export function setRightPanel (state, rightPanel) {
  state.dynamic.rightPanel = rightPanel
}

/**
 * @param state
 * @param typeBlueprintSelect {string}
 */
export function setTypeBlueprintSelect (state, typeBlueprintSelect) {
  state.dynamic.typeBlueprintSelect = typeBlueprintSelect
}

/**
 * @param state
 * @param dialogBlueprint {boolean}
 */
export function setDialogBlueprint (state, dialogBlueprint) {
  state.dynamic.dialogBlueprint = dialogBlueprint
}

/**
 * @param state
 * @param dialogImport {boolean}
 */
export function setDialogImport (state, dialogImport) {
  state.dynamic.dialogImport = dialogImport
}
