import { uid } from 'quasar'

/**
 * @param state
 * @param commit
 * @param door
 */
export function generateDoor ({
  state,
  commit
}, door) {
  // Test de doublon
  if (!state.doors.find(d => d.x === door.x && d.y === door.y)) {
    // Ajout de la porte avec la génération de l'id
    commit('addDoor', { id: uid(), ...door })
  }
}
