import { createNamespacedHelpers } from 'vuex'

const mapMappers = createNamespacedHelpers('map')

export default {
  computed: {
    ...mapMappers.mapGetters([
      'getMouseX',
      'getMouseY',
      'getCoordX',
      'getCoordY',

      'getMapData',

      'getDoors'
    ]),
    mouseX: {
      get: function () {
        return this.getMouseX
      },
      set: function (val) {
        this.setMouseX(val)
      }
    },
    mouseY: {
      get: function () {
        return this.getMouseY
      },
      set: function (val) {
        this.setMouseY(val)
      }
    },
    coordX: {
      get: function () {
        return this.getCoordX
      },
      set: function (val) {
        this.setCoordX(val)
      }
    },
    coordY: {
      get: function () {
        return this.getCoordY
      },
      set: function (val) {
        this.setCoordY(val)
      }
    },
    mapWidth: {
      get: function () {
        return this.getMapData.w
      },
      set: function (value) {
        this.setMapWidth(value)
      }
    },
    mapHeight: {
      get: function () {
        return this.getMapData.h
      },
      set: function (value) {
        this.setMapHeight(value)
      }
    }
  },
  methods: {
    ...mapMappers.mapActions([
      'generateDoor'
    ]),
    ...mapMappers.mapMutations([
      'setMouseX',
      'setMouseY',
      'setCoordX',
      'setCoordY',

      'setMapWidth',
      'setMapHeight',

      'addDoor',
      'deleteDoor',
      'updateDoor'
    ])
  }
}
