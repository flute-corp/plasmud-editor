import CONSTS from '../consts'

export default function () {
  return {
    select: {
      blueprint: null
    },
    refs: {
      types: ['item', 'actor', 'placeable'],
      subtypes: CONSTS,
      races: ['animal', 'goblinoïde', 'dragon', 'humain'],
      genders: ['féminin', 'masculin', 'robot', 'alien'],
      ranks: [{
        id: 0,
        rank: 'normal'
      }, {
        id: 1,
        rank: 'veteran'
      }, {
        id: 2,
        rank: 'boss'
      }]
    },
    blueprints: []
  }
}
