import { uid, LocalStorage } from 'quasar'

/**
 * @param commit
 * @param id {uid}
 * @param name {string}
 * @param descPlaintText {string}
 * @param tag {string}
 * @param weight {number}
 * @param subtype {string}
 * @param uname {string}
 * @param udescPlaintText {string}
 * @param stackable {boolean}
 * @param identified {boolean}
 * @param inventory {boolean}
 * @param lock {{locked: boolean, difficulty: number, discardKey: boolean, code: string, key: string}}
 */
export function submitItem ({
  commit
}, {
  id,
  name,
  descPlaintText,
  tag,
  weight,
  subtype,
  uname,
  udescPlaintText,
  stackable,
  identified,
  inventory,
  lock
}) {
  const blueprint = {
    id: id || uid(),
    type: 'item',
    name,
    desc: descPlaintText.split('\n'),
    tag,
    weight,

    subtype,
    uname,
    udesc: udescPlaintText.split('\n'),

    stackable,
    identified,
    inventory,
    lock: {
      locked: lock.locked,
      difficulty: lock.difficulty < 0 ? null : lock.difficulty,
      discardKey: lock.discardKey,
      code: lock.code,
      key: lock.key
    },
    buc: 'u'
  }
  commit('upsertBlueprintItem', blueprint)
  return blueprint
}

/**
 * @param commit
 * @param id {uid}
 * @param name {string}
 * @param descPlaintText {string}
 * @param tag {string}
 * @param weight {number}
 * @param lock {{locked: boolean, difficulty: number}}
 * @param traps {{armed: boolean, difficulty: number}}
 */
export function submitPlaceable ({
  commit
}, {
  id,
  name,
  descPlaintText,
  weight,
  tag,
  lock,
  traps
}) {
  const blueprint = {
    id: id || uid(),
    type: 'placeable',
    name,
    desc: descPlaintText.split('\n'),
    tag,
    weight,

    inventory: true,
    lock,
    traps
  }
  commit('upsertBlueprintPlaceable', blueprint)
  return blueprint
}

/**
 * @param commit
 * @param id {uid}
 * @param name {string}
 * @param descPlaintText {string}
 * @param tag {string}
 * @param weight {number}
 * @param race {string}
 * @param gender {string}
 * @param rank {number}
 */
export function submitActor ({
  commit
}, {
  id,
  name,
  descPlaintText,
  tag,
  weight,
  race,
  gender,
  rank
}) {
  const blueprint = {
    id: id || uid(),
    type: 'actor',
    name,
    desc: descPlaintText.split('\n'),
    tag,
    weight,

    race,
    gender,
    rank,

    inventory: true
  }
  commit('upsertBlueprintActor', blueprint)
  return blueprint
}

/**
 * Sauvegarde des blueprints dans le LocalStorage
 * @param state
 */
export function saveBlueprints ({ state }) {
  LocalStorage.set('blueprints', state.blueprints)
}

/**
 * Charge les blueprints du LocalStorage
 * @param commit
 */
export function loadBlueprints ({ commit }) {
  if (LocalStorage.has('blueprints')) {
    const blueprints = LocalStorage.getItem('blueprints')
    commit('setBlueprints', blueprints)
  }
}
