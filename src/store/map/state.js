export default function () {
  return {
    map: {
      w: 0,
      h: 0
    },
    mouse: {
      x: 0,
      y: 0,
      cx: 0,
      cy: 0
    },
    doors: []
  }
}
