export const getMouseX = state => state.mouse.x
export const getMouseY = state => state.mouse.y
export const getCoordX = state => state.mouse.cx
export const getCoordY = state => state.mouse.cy
export const getMapData = state => state.map

export const getDoors = state => state.doors
