export default function () {
  return {
    dynamic: {
      rightPanel: false,
      typeBlueprintSelect: '',
      dialogBlueprint: false,
      dialogImport: false
    },
    conf: {}
  }
}
