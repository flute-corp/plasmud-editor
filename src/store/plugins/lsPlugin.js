const localStoragePlugin = function () {
  return store => {
    store.subscribeAction((action, payload) => {
      switch (action.type) {
        default:
      }
    })

    store.subscribe((mutation, state) => {
      switch (mutation.type) {
        default:
      }
    })
  }
}

export default localStoragePlugin
